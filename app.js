var express = require('express')
var bodyParser = require('body-parser')
var mongoose = require('mongoose')

// var appProducts = require('./routes/products')
var appProducts = require('./api/products/products.routers')

var app = express()

// connection mongoose
mongoose.connection.openUri('mongodb://localhost:27017/tiendita', (err, res) => {
  if (err) throw error
  console.log('mongodb \x1b[32m%s\x1b[0m', 'tiendita online');
})


// body parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// routes
app.use('/v1/products', appProducts)

app.listen(3000, () => {

  console.log('Listen port 3000: \x1b[32m%s\x1b[0m', 'Online')
})
