var express = require('express')
var Products = require('./products.model')

var app = express()

// =====================================
// all products
// =====================================

app.get('/all-products/:page', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  let perPage = 9
  let page = req.params.page || 1
  console.log('req', req.params);
  Products.find({})
  .skip((perPage * page) - perPage)
  .limit(perPage)
  .exec((err, products) => {
    Products.count().exec((err, count) => {
      if (err) return next(err)
      return res.json({
        current: page,
        pages: Math.ceil(count / perPage),
        products
      })
    })
  })
})

// =====================================
// save products
// =====================================

app.post('/add-products', (req, res, next) => {

  var body = req.body

  var products = new Products({
    name: body.name,
    description: body.description,
    price: body.price
  })

  products.save((err, productsSave) => {
    if (err) {
      res.status(400).json({
        status: false,
        message: 'error al crear el producto'
      })
    }
    res.status(200).json({
      status: 200,
      message: 'Producto creado',
      product: productsSave
    })
  })
})

// =====================================
// update products
// =====================================

app.put('/update-products/:id', (req, res) => {

  var id = req.params.id
  var body = req.body

  Products.findById(id, (err, product) => {
    if (err) {
      res.status(500).json({
        status: 500,
        err
      })
    }

    if (!product) {
      res.status(400).json({
        status: 400,
        message: 'El producto con el id ' + id + 'no existe'
      })
    }

    product.name = body.name
    product.description = body.description
    product.price = body.price

    product.save((err, productSave) => {
      if (err) {
        res.status(500).json({
          status: 500,
          message: 'Error al actulizar el producto'
        })
      }
      res.status(200).json({
        status: 200,
        product: productSave
      })
    })

  })
})


// =====================================
// delete products
// =====================================

app.delete('/:id', (req, res) => {

  var id = req.params.id

  Products.findByIdAndRemove(id, (err, productDelete) => {
    if (err) {
      res.status(500).json({
        status: 500,
        message: 'Error al intentar borrar el usuario'
      })
    }

    if (!productDelete) {
      res.status(400).json({
        status: 400,
        message: 'No existe ningun usuario con ese id'
      })
    }

    res.status(200).json({
      status: 200,
      message: 'Producto borrado'
    })
  })
})

module.exports = app
