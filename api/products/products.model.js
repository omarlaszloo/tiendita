var mongoose = require('mongoose')


var Schema = mongoose.Schema


var productsSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Es necesario agregar el nombre del producto']
  },
  description: {
    type: String,
    required: [true, 'Es necesario agregar una descripción del producto']
  },
  price: {
    type: String,
    required: [true, 'Es importante agregar el precio del producto']
  },
  img: {
    type: String,
    required: [false]
  }
})

module.exports = mongoose.model('Products', productsSchema)
